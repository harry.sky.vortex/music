import { SvgIconProps } from '@material-ui/core/SvgIcon';

export interface ScreenData {
  width: number;
  height: number;
  isPortrait: boolean;
  isMobile: boolean;
}

export interface LinkData {
  readonly url: string;
  readonly icon?: React.ReactElement<SvgIconProps>;
  readonly iconSide?: 'left' | 'right';
  readonly text?: string;
}

export interface AppInfo {
  name: string;
  creator: string;
  frontendVersion: string;
  frontendTechnology: string[];
  backendVersion: string;
  backendTechnology: string[];
  servedVia: string;
}
