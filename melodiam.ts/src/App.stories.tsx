import * as React from 'react';
import { storiesOf } from '@storybook/react';

storiesOf('Button', module)
  .add('with text', () => <button>Hello Button</button>)
  .add('with some emoji', () => {
    return (
      <button>
        <span role="img" aria-label="so cool">
          😀 😎 👍 💯
        </span>
      </button>
    );
  });
